package ru.t1.avfilippov.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

    void completeProjectById();

    void completeProjectByIndex();

    void showProjectById();

    void showProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void startProjectById();

    void startProjectByIndex();

    void changeStatusProjectById();

    void changeStatusProjectByIndex();

}
