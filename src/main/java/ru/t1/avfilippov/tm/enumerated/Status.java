package ru.t1.avfilippov.tm.enumerated;

public enum Status {

    NOT_STARTED("Not Started"),
    IN_PROGRESS("In Progress"),
    COMPLETED("Completed");

    public static String toName(final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    public static Status toStatus(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Status status : values()) {
            if (status.name().equals(value)) return status;
        }
        return null;
    }

    private final String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public final String getDisplayName() {
        return displayName;
    }

}
